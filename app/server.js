import config from './backend/config.js';
import http from 'http'
import mongoose from 'mongoose';
import ExpressServer from './backend/expressServer.js';
let priv;
if(!process.env.MONGOLAB_URI){
	priv = require('../private/config.js').default;
}
let port = process.env.PORT || config.port;
let app = new ExpressServer();
let server = http.createServer(app.expressServer);

let dbUri = process.env.MONGOLAB_URI || priv.db || config.db.dev;

mongoose.connect(dbUri, (err) => {
    if(err){
        console.log("ERROR: " + err);
    }else{
        console.log("Conectado á BD con exito");
    }
});

server.listen(port, () => {
    console.log("Magic on port " + port);
});
