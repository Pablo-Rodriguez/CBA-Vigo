
import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

import Base from './base.js';
import Home from './home.js';
import Noticias from './noticias.js';
import NoticiaConcreta from './noticiaConcreta.js';
import Contacta from './contacta.js';
import About from './about.js';

export default class AppRouter extends React.Component {

    render () {
        return (
            <Router history={ hashHistory }>
                <Route path="/" component={ Base }>
                    <IndexRoute component={ Home }></IndexRoute>
                    <Route path="noticias" component={ Noticias }></Route>
                    <Route path="noticia/:titulo" component={ NoticiaConcreta }></Route>
                    <Route path="contacta" component={ Contacta }></Route>
                    <Route path="about" component={ About }></Route>
                </Route>
            </Router>
        );
    }
}
