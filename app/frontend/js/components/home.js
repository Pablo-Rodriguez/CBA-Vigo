
import React from 'react';
import request from 'superagent';

import Partido from './partido.js';

export default class Home extends React.Component {
    constructor (props) {
        super(props);
        this.state = { partidos: [] };
    }

    componentDidMount () {
        request.get('/partidos')
            .end((err, response) => {
                if(!err && response.statusCode === 200){
                    this.setState({ partidos: response.body })
                }
            });
    }

    render () {
        return (
            <section className="home">
    			<article className="landing">
    				<h2>Bienvenido</h2>
    				<figure>
    					<img src="../img/logo.png" width="96" height="96"
                            alt="CBA Vigo logo" title="CBA Vigo logo"/>
    				</figure>
    				<h1>CBA Vigo</h1>
    			</article>
    			<article className="last-results wrap" id="last-results">
    				<h2>Últimos resultados</h2>
    				<div className="cont">
    					<div className="cbavigo-a">
    						<h3>CBA Vigo</h3>
    						<ul className="results" id="res-a">
                                {this.state.partidos.map((partido) => {
                                    return (
                                        <Partido
                                            key={ partido._id }
                                            info={ partido.info }
                                    />);
                                })}
    						</ul>
    					</div>
    				</div>
    			</article>
    			<footer className="footer">
    				<h4>Copyright © <strong>CBA Vigo</strong>. 2015 • All rights reserved.</h4>
    			</footer>
    		</section>
        );
    }
}
