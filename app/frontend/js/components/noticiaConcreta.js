
import React from 'react';
import request from 'superagent';

export default class NoticiaConcreta extends React.Component {
    constructor (props) {
        super(props);
        this.state = { noticia: {} };
    }

    componentDidMount () {
        //Conseguir la noticia a partir de this.props.params.titulo
        let titulo = this.props.params.titulo;
        let noticia = localStorage.getItem(titulo);
        if(noticia !== undefined && noticia !== null){
            this.setState({ noticia: JSON.parse(noticia) })
        }else{
            request.get(`/noticias/concreta/${titulo}`)
                .end((err, response) => {
                    if(!err && response.statusCode === 200){
                        let body = response.body;
                        if(body.success && body.data){
                            let str = JSON.stringify(body.data);
                            this.setState({ noticia: body.data });
                            window.localStorage.setItem(titulo, str);
                        }
                    }
                });
        }
    }

    render () {
        return (
            <section className="novaConcreta">
    			<figure>
    				<img
                        className="img"
                        width="320px"
                        height="214px"
                        src={ this.state.noticia.src }
                        alt={ this.state.noticia.alt }
                        title={ this.state.noticia.title }
                    />
    			</figure>
    			<div className="corpo">
    				<h2>{ this.state.noticia.titulo }</h2>
    				<div
                        dangerouslySetInnerHTML={{__html: this.state.noticia.cuerpo}}
                    ></div>
    			</div>
    		</section>
        );
    }
}
