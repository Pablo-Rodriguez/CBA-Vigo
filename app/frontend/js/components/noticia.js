
import React from 'react';
import { Link } from 'react-router';

export default class Noticia extends React.Component {
    render () {
        let titulo = this.props.titulo.split(' ').join('-');
        let url = `/noticia/${ titulo }`;
        return (
            <article className="new img">
                <Link to={ url }>
                    <div className="cuerpo">
            			<figure className="img">
            				<img
                                className="img"
                                src={ this.props.src }
                                width="320"
                                height="214"
                                alt={ this.props.alt }
                                title={ this.props.title }
                            />
            				<div className="cont">
            					<h2 className="titulo">{ this.props.titulo }</h2>
            				</div>
            			</figure>
            		</div>
            		<div className="adornos">
            			<div className="adorno1 adorno"></div>
            			<div className="adorno2 adorno"></div>
            			<div className="adorno3 adorno"></div>
            		</div>
                </Link>
            </article>
        );
    }
}
