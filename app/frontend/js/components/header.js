
import React from 'react';

export default class Header extends React.Component {
    render () {
        return (
            <header className="header border-bottom">
    			<a
                    className="icon-arrow-left boton-nav"
                    onClick={ this.props.onClick }
                    ></a>
    			<figure>
    				<img src="../img/logo.png"
                        width="48px"
                        height="48px"
                        className="logo"
                        alt="CBA Vigo logo"
                        title="CBA Vigo logo" />
    			</figure>
    		</header>
        );
    }
}
