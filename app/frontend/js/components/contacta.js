
import React from 'react';

export default class Contacta extends React.Component {
    render () {
        return (
            <section className="contacta">
    			<div className="content">
    				<h2>Buscamos</h2>
    				<h3>Patrocinadores</h3>
    				<a className="button">Contacta</a>
    				<span className="icon-ball"></span>
    				<div className="adorno"></div>
    			</div>
    			<div className="redes">
    				<p> Escribenos a cbavigo@gmail.com</p>
    				<div className="add-on addleft">
    					<span className="icon-twitter"></span>
    					<a href="https://twitter.com/cbavigo" className="btn twitter">@cbavigo</a>
    				</div>
    				<div className="add-on addleft">
    					<span className="icon-facebook"></span>
    					<a href="https://www.facebook.com/profile.php?id=100009902918564" className="btn facebook">CBA Vigo</a>
    				</div>
    			</div>
    		</section>
        );
    }
}
