
import React from 'react';

export default class About extends React.Component {
    render () {
        let foto = window.screen.width < 450 ? false : true;
        let image, adorno;

        if(foto){
            image = (
                <figure className="img">
					<img
                        className="about-image img"
                        src="img/equipo-min.jpg"
                        width="320px"
                        height="240px"></img>
				</figure>
            );
            adorno = (
                <div className="aux"></div>
            );
        }

        return (
            <section className="about">
    			{image}
    			<div className="texto">
    				<div className="titulo">
    					<p className="icon-ball adorno1"></p>
    					<h2>CBA Vigo</h2>
    					<p className="icon-ball adorno2"></p>
    				</div>
    				<p className="about-text">Somos el CBA Vigo, un club modesto en cuanto a presupuesto pero enorme en cuanto a personas. Nacimos en 2015 con la ilusión de crear un equipo en el que todos son importantes y para librarnos de la molesta burocracia de las instituciones deportivas tradicionales. Si eres jugador y te gusta como suena, mándanos un mail a cbavigo@gmail.com o contáctanos en las redes sociales y veremos si podemos hacerte un hueco, todo el mundo es bienvenido. Si por otro lado no te interesa jugar pero te gustaría apoyar esta iniciativa, nos encantaría escuchar tus propuestas, ya sean acerca de patrocinios, donaciones o cualquier cosa que nos pueda ayudar. Gracias y NOS VEMOS EN LA PISTA :)</p>
    				<p className="icon-ball adorno3"></p>
    			</div>
    			{adorno}
    		</section>
        );
    }
}
