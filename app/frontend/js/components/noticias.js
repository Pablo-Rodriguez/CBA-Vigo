
import React from 'react';
import request from 'superagent';

import Noticia from './noticia.js';

export default class Noticias extends React.Component {
    constructor (props) {
        super(props);
        this.state = { noticias: [] };
    }

    componentDidMount () {
        request.get('/noticias')
            .end((err, response) => {
                if(!err && response.statusCode === 200){
                    let body = response.body;
                    this.setState({ noticias: body });
                    body.forEach((noticia) => {
                        let titulo = noticia.titulo.split(' ').join('-');
                        let str = JSON.stringify(noticia);
                        window.localStorage.setItem(titulo, str);
                    });
                }
            });
    }

    render () {
        let noticias;
        if(this.state.noticias.length > 0){
            noticias = this.state.noticias.map((noticia) => {
                return (
                    <Noticia
                        key={ noticia._id }
                        src={ noticia.src }
                        alt={ noticia.alt }
                        title={ noticia.title }
                        titulo={ noticia.titulo }
                    />
                );
            });
        }else{
            noticias = (
                <h3 className="loading">Cargando...</h3>
            );
        }

        return (
            <section className="noticias">
    			<h2 className="title">Noticias</h2>
    			<div className="news-container">
    				{ noticias }
    			</div>
    			<div className="adorno-cierre"></div>
    		</section>
        );
    }
}
