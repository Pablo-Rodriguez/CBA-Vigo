
import React from 'react';
import $ from 'jquery';
import { Link } from 'react-router';

export default class Nav extends React.Component {

    close () {
        if(window.screen.width < 600){
            setTimeout(() => {
                $('section').addClass('movedFast');
            }, 10);
            setTimeout(() => {
                this.move('close');
                $('section').removeClass('movedFast');
			}, 100);
		}
    }

    open () {
        this.move('open');
    }

    move (how) {
        let fn = {
            'open': 'addClass',
            'close': 'removeClass',
            'toggle': 'toggleClass'
        }[how.toLowerCase()];

        $('.header')[fn]('moved');
		$('.nav')[fn]('moved');
		$('section')[fn]('moved');
		$('.boton-nav')[fn]('rotated');
    }

    render () {
        return (
            <nav className="nav">
    			<ul className="nav-list" onClick={ this.close.bind(this) }>
    				<li className="nav-list-element"><Link to="" className="nav-list-element-link">Home</Link></li>
    				<li className="nav-list-element"><Link to="noticias" className="nav-list-element-link">Noticias</Link></li>
    				<li className="nav-list-element"><Link to="contacta" className="nav-list-element-link">Contacta</Link></li>
    				<li className="nav-list-element"><Link to="about" className="nav-list-element-link">About</Link></li>
    				<li className="nav-list-element centered padding no-border media">
    					<div className="add-on addleft">
    						<span className="icon-twitter"></span>
    						<a href="https://twitter.com/cbavigo" className="btn twitter">@cbavigo</a>
    					</div>
    					<div className="add-on addleft">
    						<span className="icon-facebook"></span>
    						<a href="https://www.facebook.com/profile.php?id=100009902918564" className="btn facebook">CBA Vigo</a>
    					</div>
    				</li>
    			</ul>
    		</nav>
        );
    }
}
