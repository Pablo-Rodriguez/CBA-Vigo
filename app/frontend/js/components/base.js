
import React from 'react';
import $ from 'jquery';
import Hammer from 'hammerjs';

import Header from './header.js';
import Nav from './nav.js';

export default class Base extends React.Component {
    onClick () {
        let nav = this.refs.nav;
        nav.move('toggle');
    }

    componentDidMount () {
        let body = document.querySelector('body');
		let canvas = new Hammer(body);
        let nav = this.refs.nav;

		if(window.screen.width < 600){
			canvas.get('pan').set({
				direction: Hammer.DIRECTION_HORIZONTAL,
				threshold: 150
			});

			canvas.on('panright', nav.open.bind(nav));
			canvas.on('panleft', nav.close.bind(nav));
		}

		$(window).resize(() => {
			if(window.screen.width < 600){
				canvas.on('panright', nav.open.bind(nav));
				canvas.on('panleft', nav.close.bind(nav));
			}else{
				canvas.off('panright');
				canvas.off('panleft');
			}
		});
    }

    render () {
        return (
            <div>
                <Header onClick={ this.onClick.bind(this) }></Header>
                <Nav ref="nav"></Nav>
                <main>
                    { this.props.children }
                </main>
            </div>
        );
    }
}
