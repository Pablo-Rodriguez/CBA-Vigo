var Novas = require('./collections/novas');

var $btn_get = $('#only-get'),
	$btn_post = $('#only-post'),
	$btn_put = $('#only-put'),
	$btn_delete = $('#only-delete'),
	$novas_get = $('#Novas-get'),
	$novas_post = $('#Novas-post'),
	$novas_put = $('#Novas-put'),
	$novas_delete = $('#Novas-delete'),
	$match_get = $('#Partidos-get'),
	$match_post = $('#Partidos-post'),
	$match_put = $('#Partidos-put'),
	$match_delete = $('#Partidos-delete');

var View = Backbone.View.extend({
	template: $('#template').html(),

	initialize: function () {
		this.listenTo(this.model, 'change', this.render, this);
	},

	render: function () {
		var output = Mustache.render(this.template, this.model.toJSON());
		return output;
	}
});

var List = Backbone.View.extend({
	el: '#Novas-get',

	events: {
		'click input.btn': 'recibir'
	},

	initialize: function () {
		this.listenTo(this.collection, 'add', this.addOne, this);
	},

	addOne: function (nova) {
		var view = new View({
			model: nova
		});

		this.$el.children('ul#lista-novas').prepend(view.render());
	},

	recibir: function () {
		this.collection.fetch();
	},

	render: function () {
		this.collection.forEach(function (nova) {
			this.addOne(nova);
		});
	}
});

var Match = Backbone.Model.extend();
var MatchCol = Backbone.Collection.extend({
	model: Match,
	url: '/partidos',

	parse: function (response) {
		response.forEach(function (elem) {
			elem.id = elem._id;
		});

		return response;
	}
});

var MatchView = Backbone.View.extend({
	template: $('#tmpl-match').html(),

	initialize: function () {
		this.listenTo(this.model, 'change', this.render, this);
	},

	render: function () {
		var output = Mustache.render(this.template, this.model.toJSON());
		return output;
	}
});

var MatchList = Backbone.View.extend({
	el: '#Partidos-get',

	events: {
		'click input.btn': 'recibir'
	},

	initialize: function () {
		this.listenTo(this.collection, 'add', this.addOne, this);
	},

	addOne: function (match) {
		var view = new MatchView({
			model: match
		});

		this.$el.children('ul#lista-partidos').prepend(view.render());
	},

	render: function () {
		this.collection.forEach(function (match) {
			this.addOne(match);
		});
	},

	recibir: function () {
		this.collection.fetch();
	}
});

$(function () {
	
	var novas = new List({
		collection: new Novas()
	});

	var partidos = new MatchList({
		collection: new MatchCol()
	});

	$match_post.addClass('hide');
	$match_put.addClass('hide');
	$match_delete.addClass('hide');
	$novas_post.addClass('hide');
	$novas_put.addClass('hide');
	$novas_delete.addClass('hide');

	$btn_get.click(function () {
		$novas_get.removeClass('hide');
		$novas_post.addClass('hide');
		$novas_put.addClass('hide');
		$novas_delete.addClass('hide');

		$match_get.removeClass('hide');
		$match_post.addClass('hide');
		$match_put.addClass('hide');
		$match_delete.addClass('hide');
	});
	$btn_post.click(function () {
		$novas_get.addClass('hide');
		$novas_post.removeClass('hide');
		$novas_put.addClass('hide');
		$novas_delete.addClass('hide');

		$match_get.addClass('hide');
		$match_post.removeClass('hide');
		$match_put.addClass('hide');
		$match_delete.addClass('hide');
	});
	$btn_put.click(function () {
		$novas_get.addClass('hide');
		$novas_post.addClass('hide');
		$novas_put.removeClass('hide');
		$novas_delete.addClass('hide');

		$match_get.addClass('hide');
		$match_post.addClass('hide');
		$match_put.removeClass('hide');
		$match_delete.addClass('hide');
	});
	$btn_delete.click(function () {
		$novas_get.addClass('hide');
		$novas_post.addClass('hide');
		$novas_put.addClass('hide');
		$novas_delete.removeClass('hide');

		$match_get.addClass('hide');
		$match_post.addClass('hide');
		$match_put.addClass('hide');
		$match_delete.removeClass('hide');
	});

});