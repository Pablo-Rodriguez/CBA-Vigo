
import React from 'react';
import { render } from 'react-dom';
import AppRouter from './components/app';

render(<AppRouter />, document.getElementById('app'))
