var Partido = require('../models/partido')

module.exports = Backbone.Collection.extend({
	model: Partido,
	url: '/partidos',

	parse: function (response) {
		response.forEach(function (elem) {
			elem.id = elem._id;
		});

		return response;
	}
});