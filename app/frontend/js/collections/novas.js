var Nova = require('../models/nova');

module.exports = Backbone.Collection.extend({
	model: Nova,
	url: '/noticias',

	parse: function (response) {
		response.forEach(function (elem) {
			elem.id = elem._id;
		});

		return response;
	}
});