module.exports = Backbone.View.extend({
	template: $('#about').html(),
	render: function () {
		var output = Mustache.render(this.template, {foto: window.screen.width < 450 ? false : true});
		$('section').remove();
		this.$el.append(output);
	}
});