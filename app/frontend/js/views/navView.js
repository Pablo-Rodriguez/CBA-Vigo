module.exports = Backbone.View.extend({
	template: $('#header-nav').html(),

	events: {
		'click .boton-nav': 'toggleNav',
		'click #home-link': 'goHome',
		'click #noticias-link': 'goNoticias',
		'click #fotos-link': 'goFotos',
		'click #contacta-link': 'goContacta',
		'click #about-link': 'goAbout'
	},

	initialize: function () {
		var body = document.querySelector('body');
		var canvas = new Hammer(body);

		if(window.screen.width < 600){
			canvas.get('pan').set({
				direction: Hammer.DIRECTION_HORIZONTAL,
				threshold: 150
			});

			canvas.on('panright', this.displayNav);
			canvas.on('panleft', this.hideNav);
		}

		$(window).resize(function () {
			if(window.screen.width < 600){
				canvas.on('panright', this.displayNav);
				canvas.on('panleft', this.hideNav);
			}else{
				canvas.off('panright');
				canvas.off('panleft');
			}
		}.bind(this));

	},

	render: function () {
		var output = Mustache.render(this.template);
		this.$el.prepend(output);
	},

	displayNav: function () {
		$('.header').addClass('moved');
		$('.nav').addClass('moved');
		$('section').addClass('moved');
		$('.boton-nav').addClass('rotated');
	},

	hideNav: function () {
		$('.header').removeClass('moved');
		$('.nav').removeClass('moved');
		$('section').removeClass('moved');
		$('.boton-nav').removeClass('rotated');
	},

	toggleNav: function() {
		$('.header').toggleClass('moved');
		$('.nav').toggleClass('moved');
		$('section').toggleClass('moved');
		$('.boton-nav').toggleClass('rotated');
	},

	goHome: function () {
		app.navigate('', {trigger: true});
		this.move();
	},

	goNoticias: function () {
		app.navigate('noticias', {trigger: true});
		this.move();
	},

	goFotos: function () {
		app.navigate('fotos', {trigger: true});
		this.move();
	},

	goContacta: function () {
		app.navigate('contacta', {trigger: true});
		this.move();
	},

	goAbout: function () {
		app.navigate('about', {trigger: true});
		this.move();
	},

	move: function () {
		if(window.screen.width < 600){
			$('section').addClass('moved');
			setTimeout(function () {
				this.toggleNav();
			}.bind(this), 10);
		}
	}
});
