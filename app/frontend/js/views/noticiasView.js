var NovaView = require('./novaView');

module.exports = Backbone.View.extend({
	template: $('#noticias').html(),

	initialize: function () {
		this.listenTo(this.collection, 'add', this.addOne, this);
	},
	addOne: function (nova) {
		let l;
		if(l = $('.loading')){
			l.remove();
		}
		var novaView = new NovaView({model: nova});
		this.$el.find('.news-container').prepend(novaView.render().el);
	},
	render: function () {
		var output = Mustache.render(this.template);
		$('section').remove();
		this.$el.append(output);

		this.collection.forEach(this.addOne, this);
	}
});