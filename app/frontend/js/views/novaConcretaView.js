module.exports = Backbone.View.extend({
	template: $('#novaConcreta').html(),
	events: {},

	initialize: function () {
		this.listenTo(this.model, 'change', this.render, this);
	},

	render: function () {
		var output = Mustache.render(this.template, this.model.toJSON());
		$('section').remove();
		this.$el.prepend(output);
	}
});