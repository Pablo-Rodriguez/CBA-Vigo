module.exports = Backbone.View.extend({
	template: $('#new').html(),
	tagName: 'article',
	className: 'new img',
	events: {
		'click .cuerpo': 'vistaConcreta'
	},

	initialize: function () {
		this.listenTo(this.model, 'change', this.render, this);
	},

	render: function() {
		var output = Mustache.render(this.template, this.model.toJSON());
		this.$el.html(output);
		return this;
	},

	vistaConcreta: function () {
		app.navigate('noticias/' + this.model.get('id'), {trigger: true});
	}
});