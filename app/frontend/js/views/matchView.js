module.exports = Backbone.View.extend({
	tagName: 'li',
	className: 'match',

	initialize: function () {
		this.listenTo(this.model, 'change', this.render, this);
	},

	render: function () {
		this.$el.html(this.model.attributes.info);

		return this;
	}
});