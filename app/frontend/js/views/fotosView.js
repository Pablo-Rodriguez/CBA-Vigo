module.exports = Backbone.View.extend({
	template: $('#fotos').html(),
	render: function () {
		var output = Mustache.render(this.template);
		$('section').remove();
		this.$el.append(output);
	}
});