module.exports = Backbone.View.extend({
	template: $('#contacta').html(),
	events: {
		'click .button': 'redes'
	},
	initialize: function () {
		var body = document.querySelector('body');
		var canvas = new Hammer(body);
		canvas.on('pantop', this.displayRedes);
		canvas.on('panbottom', this.hideRedes);
	},
	render: function () {
		var output = Mustache.render(this.template);
		$('section').remove();
		this.$el.append(output);
	},
	displayRedes: function () {
		$('.redes').addClass('up');
	},
	hideRedes: function () {
		$('.redes').removeClass('up');
	},
	redes: function () {
		$('.redes').toggleClass('up');
	}
});