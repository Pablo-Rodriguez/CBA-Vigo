module.exports = Backbone.View.extend({
	template: $('#home').html(),
	render: function () {
		var output = Mustache.render(this.template);
		$('section').remove();
		this.$el.append(output);
	}
});