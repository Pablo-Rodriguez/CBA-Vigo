var MatchView = require('./matchView');

module.exports = Backbone.View.extend({

	initialize: function () {
		this.listenTo(this.collection, 'add', this.addOne, this);
	},

	addOne: function (match) {
		var view = new MatchView({model: match});
		var where;
		var equipo = match.attributes.equipo;
		if(equipo === 'a' || equipo === 'A'){
			where = '#res-a';
		}else if(equipo === 'b' || equipo === 'B'){
			where = '#res-b';
		}else{
			return;
		}
		this.$el.find($(where)).prepend(view.render().el);
	},

	render: function () {
		this.$el = $(this.el);
		this.collection.forEach(function (match) {
			this.addOne(match);
		}.bind(this));
	}
});