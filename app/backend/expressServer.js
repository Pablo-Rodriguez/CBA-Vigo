import path from 'path';

import express from 'express';
import bodyParser from 'body-parser';
import multipart from 'connect-multiparty';
import methodOverride from 'method-override';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import router from './MVC/router.js';
import jwtMid from './middlewares/jwt.js';

let ExpressServer = function (config) {
    config = config || {};

    this.expressServer = express();

    //Engines
    this.expressServer.set('views', path.join(__dirname, 'MVC', 'views', 'templates'));
    this.expressServer.set('view engine', 'jade');

    //Middleware
    this.expressServer.use(bodyParser.urlencoded({extended: true}));
    this.expressServer.use(bodyParser.json());
    this.expressServer.use(multipart());
    this.expressServer.use(methodOverride('_method'));
    this.expressServer.use(cookieParser());
    this.expressServer.use('/', express.static(path.join(__dirname, 'public')));
    this.expressServer.use(morgan('dev'));
    this.expressServer.use(jwtMid());


    //Routes parser
    this.parseRoutes();
};

//Enrutador que recorre os prototipos dos distintos controladores
//e en funcion do nome dos metodos crea as rutas e llas pasa a express
ExpressServer.prototype.parseRoutes = function () {
    console.log('\n********************PARSING URL\'s********************');
    for(let controller in router){
        this.initController(controller);
        for(let resource in router[controller].prototype){
            let arr = resource.split('_');
            let method = arr[0];
            if(['get', 'post', 'put', 'delete'].indexOf(method) == -1){
                continue;
            }
            let action = arr[1];
            let data = arr[2];
            data = ( method === 'get' && data === 'data') ? ':data' : '';
            if(controller === '*' && action !== 'root'){
                continue;
            }
            let url = (controller === '*' && action === 'root') ? '*' :
                ( controller === 'home' && action === 'root' ) ? '/' :
                action === 'root' ? '/' + controller + '/' :
                '/' + controller + '/' + action + '/' + data;
            this.route(controller, resource, method, url);
        }
    }
    console.log('*****************************************************\n');
};

//Inicializa un controlador
ExpressServer.prototype.initController = function (controller) {
    this[controller] = new router[controller]();
};

//Pasalle a express o metodo a ruta e o metodo do controlador
ExpressServer.prototype.route = function (controller, resource, method, url) {
    console.log('\t' + method + '\t' + url);
    this.expressServer[method](url, (req, res) => {
        let conf = {
            req: req,
            res: res,
            resource: resource
        };

        this[controller].response(conf);
    });
};

export default ExpressServer;