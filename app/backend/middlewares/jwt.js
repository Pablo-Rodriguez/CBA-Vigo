
import jwt from 'jsonwebtoken';
import conf from '../config.js';

let pass = {
    get: ['/', '/favicon.ico', '/admin/login', '/noticias', '/partidos', '/noticias/concreta/*'],
    post: ['/usuarios/login'],
    put: [],
    delete: []
};

let canPass = function (req) {
    let method = req.method.toLowerCase();
    let url = req.url;
    if(pass[method].indexOf(url) !== -1){
        return true;
    }else{
        url = url.split('/');
        url[url.length - 1] = '*';
        url = url.join('/');
        if(pass[method].indexOf(url) !== -1){
            return true;
        }
    }
    return false;
}

let middleware = function(config) {
    config = config || {};
    return function (req, res, next) {
        if(canPass(req)){
            next();
        }else{
            let token = req.cookies.cbavigotoken || req.body.token;
            if(token){
                let secret = process.env.JWT_SECRET || conf.jwt;
                jwt.verify(token, secret, (err, decoded) => {
                    if(err){
                        res.redirect('/admin/login');
                    }else{
                        req.decoded = decoded;
                        next();
                    }
                });
            }else{
                console.log('no token');
                res.redirect('/admin/login');
            }
        }
    }
}

export default middleware;