//Controlador da noticia
import Model from '../models/noticia.js';

let Noticia = function (config) {
    config = config || {};

    this.model = new Model();

    this.response = (conf) => {
        this[conf.resource](conf.req, conf.res);
    }
};

Noticia.prototype.get_root = function (req, res) {
    this.model.all(req.body.page).exec(function (err, novas) {
        if(err){
            console.log('ERROR: ' + err);
        }else{
            res.send(novas);
            console.log('Novas enviadas');
        }
        res.end();
    });
};

Noticia.prototype.get_concreta_data = function(req, res) {
    let titulo = req.params.data.split('-').join(' ');
    res.setHeader("Content-type", "application/json");
    this.model.byTitle(titulo).exec((err, noticia) => {
        if(err){
            console.log('Error: ' + err);
            res.json({
                success: false
            });
        }else if(!noticia){
            res.json({
                success: true,
                data: false
            });
        }else{
            res.json({
                success: true,
                data: noticia
            });
        }
    });
};

Noticia.prototype.post_root = function (req, res) {
    let model = {
        alt: req.body.alt,
        title: req.body.title,
        titulo: req.body.titulo,
        cuerpo: req.body.cuerpo,
        img: req.files.image
    };

    this.model.new(model).then(() => {
        res.redirect('back');
    })
    .catch((err) => {
        if(err.code === 11000){
            res.send('Ya existe una noticia con ese titulo');
        }else{
            res.send('Hemos tenido un problema');
        }
    });
};

Noticia.prototype.put_root = function (req, res) {
    let model = {
        id: req.body.id,
        alt: req.body.alt,
        title: req.body.title,
        titulo: req.body.titulo,
        cuerpo: req.body.cuerpo,
        img: req.files.image
    };
    this.model.update(model).then(() => {
        console.log(`Se actualizo la noticia ${model.id}`);
        res.redirect('back');
    })
    .catch((err) => {
        if(err.mycode === 1){
            res.send('La noticia que intenta actualizar no existe');
        }else{
            res.send('Hemos tenido un problema');
        }
    });
};

Noticia.prototype.delete_root = function (req, res) {
    this.model.remove(req.body.id).then(() => {
        console.log('Nova eliminada');
        res.redirect('back')
    })
    .catch((err) => {
        console.log('Non se eliminou a nova');
        if(err.mycode === 1){
            res.send('La noticia que intenta eliminar no existe');
        }else{
            res.send('Hemos tenido un problema')
        }
    });
};

export default Noticia;
