//Controlador do partido
import Model from '../models/partido.js';

let Partido = function (config) {
    config = config || {};

    this.model = new Model();

    this.response = (conf) => {
        this[conf.resource](conf.req, conf.res);
    }
};

Partido.prototype.get_root = function (req, res) {
    this.model.all(req.body.page).exec(function (err, partidos) {
        if(err){
            console.log('ERROR: ' + err);
            res.write('Hemos tenido un problema');
        }else{
            res.send(partidos);
            console.log('Partidos enviados');
        }
    });
};

Partido.prototype.post_root = function (req, res) {
    let model = {
        info: req.body.info,
        equipo: req.body.equipo
    };

    this.model.new(model).then(() => {
        console.log('Partido engadido');
        res.redirect('back');
    })
    .catch((err) => {
        console.log('ERROR: ' + err);
        res.write('Hemos tenido un problema');
    });
};

Partido.prototype.put_root = function (req, res) {
    let model = {
        info: req.body.info,
        equipo: req.body.equipo
    };
    this.model.update(model).then(() => {
        console.log('partido editado');
        res.redirect('back');
    })
    .catch((err) => {
        if(err.mycode === 1){
            res.write('El partido que intentas editar no existe');
            console.log('El partido no existe');
        }else{
            res.write('Hemos tenido un problema');
        }
    });
};

Partido.prototype.delete_root = function (req, res) {
    this.model.remove(req.body.id).then(() => {

    })
    .catch((err) => {
        if(err.mycode === 1){
            res.write('El partido que intenta borrar no existe');
        }else{
            res.write('Hemos tenido un problema');
        }
        console.log('No se ha podido borrar');
    });
};

export default Partido;