
let Defaults = function (config) {
    config = config || {};

    this.response = (conf) => {
        this[conf.resource](conf.req, conf.res);
    }
};

export default Defaults;