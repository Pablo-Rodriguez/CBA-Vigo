import View from '../views/admin.js';

let Admin = function (config) {
    config = config || {};

    this.view = new View();

    this.response = (conf) => {
        this[conf.resource](conf.req, conf.res);
    }
}

Admin.prototype.get_root = function(req, res) {
    this.view.admin(res);
};

Admin.prototype.get_login = function(req, res) {
    this.view.login(res);
};

export default Admin;