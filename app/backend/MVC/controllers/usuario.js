//Controlador do usuario
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import Model from '../models/usuario.js';
import config from '../../config.js';

let Usuario = function (config) {
    config = config || {};

    this.model = new Model();

    this.response = (conf) => {
        this[conf.resource](conf.req, conf.res);
    }
};

Usuario.prototype.get_root = function (req, res) {
    this.model.all(req.body.page).exec(function (err, users) {
        if(err){
            console.log('ERROR: ' + err);
        }else{
            res.send(users);
        }
        res.end();
    });
};

Usuario.prototype.post_root = function (req, res) {
    this.model.new(model).then(() => {
        res.redirect('back');
        console.log('usuario creado');
    })
    .catch((err) => {
        if(err.code === 11000){
            res.send("Ese nombre de usuario no esta disponible.");
        }else{
            console.log('ERROR: ' + err);
            res.send("Ha ocurrido un error, vuelva a intentarlo.");
        }
    });
};

Usuario.prototype.delete_root = function (req, res) {
    this.model.remove(req.body.id).then(() => {
        res.redirect('back');
        console.log('usuario robado');
    })
    .catch((err) => {
        if(err.mycode === 1){
            res.send('El usuario que intenta borrar no existe');
        }else{
            res.send('Hemos tenido un problema');
        }
    });
};

Usuario.prototype.post_login = function(req, res) {
    this.model.byName(req.body.name).exec((err, usuario) => {
        if(err){
            console.log("ERROR: " + err);
            res.write('Hemos tenido un problema');
        }else if(!usuario){
            res.write('Ese usuario no existe');
        }else{
            if(bcrypt.compareSync(req.body.password, usuario.password)){
                //Login
                //Se quita la contraseña del objeto para que no se envie a las vistas
                delete usuario.password;
                //Se crea el token y se inscribe en jwt
                let secret = process.env.JWT_SECRET || config.jwt;
                let token = jwt.sign(usuario, secret, {
                    expiresInMinutes: 1440
                });

                res.cookie('cbavigotoken', token).redirect('/admin');
            }else{
                res.write('La contraseña no es valida');
            }
        }
    });
};

export default Usuario;