
let Admin = function (config) {
    config = config || {};
};

Admin.prototype.admin = function(res, obj) {
    obj = obj || {};
    res.render('admin', obj);
};

Admin.prototype.login = function(res, obj) {
    obj = obj || {};
    res.render('login', obj);
};

export default Admin;