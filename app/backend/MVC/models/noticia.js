
import fs from 'fs';
import cloudinary from 'cloudinary';
import Schema from './schemas/noticia.js';

// TODO -> Cambiar esta forma de importar la configuracion privada
let priv;
if(!process.env.CLOUDINARY_NAME){
    priv = require('../../../../private/config.js').default;
}

let Noticia = function (config) {
    config = config || {};

    this.cloudinary = cloudinary;
    this.cloudinary.config({
        cloud_name: process.env.CLOUDINARY_NAME || priv.cloudinary.name,
        api_key: process.env.CLOUDINARY_KEY || priv.cloudinary.key,
        api_secret: process.env.CLOUDINARY_SECRET || priv.cloudinary.secret
    });
};

Noticia.prototype.all = function (page=0) {
    return Schema.find().skip(page);
};

Noticia.prototype.byId = function (id) {
    return Schema.findById(String(id));
};

Noticia.prototype.byTitle = function (titulo) {
    return Schema.findOne({'titulo': titulo});
};

Noticia.prototype.uploadImage = function (path, id) {
    return new Promise((resolve, reject) => {
        let stream = this.cloudinary.uploader.upload_stream((result) => {
            resolve(result.secure_url);
        }, {public_id: String(id)});
        let is = fs.createReadStream(path).pipe(stream);
    });
};

Noticia.prototype.new = function(model) {
    var noticia = new Schema({
        src: "",
        alt: model.alt,
        title: model.title,
        titulo: model.titulo,
        cuerpo: model.cuerpo
    });

    return new Promise((resolve, reject) => {
        noticia.save((err) => {
            if(err){
                reject(err);
            }else{
                resolve();
            }
        });
    })
    .then(() => {
        return this.uploadImage(model.img.path, noticia._id);
    })
    .then((src) => {
        noticia.src = src;
        return;
    })
    .then(() => {
        noticia.save((err) => {
            if(err){
                throw err;
            }else{
                return;
            }
        })
    });
};

Noticia.prototype.update = function(model) {
    return new Promise((resolve, reject) => {
        Schema.findById(model.id, (err, noticia) => {
            if(err){
                err.mycode = 0;
                reject(err);
            }else if(!noticia){
                let err = {mycode:1};
                reject(err);
            }else{
                resolve(noticia);
            }
        });
    })
    .then((noticia) => {
        if(typeof model.img != 'undefined' && model.img.name != '' && model.img.size != 0){
            return new Promise((resolve, reject) => {
                this.removeImage(model.id).then(() => resolve());
            })
            .then(() => {
                return this.uploadImage(model.img.path, noticia._id);
            })
            .then((src) => {
                noticia.src = src;
                return noticia;
            });
        }else{
            return Promise.resolve(noticia);
        }
    })
    .then((noticia) => {
        if(typeof model.alt != 'undefined' && model.alt != ''){
            noticia.alt = model.alt;
        }

        if(typeof model.title != 'undefined' && model.title != ''){
            noticia.title = model.title;
        }

        if(typeof model.titulo != 'undefined' && model.titulo != ''){
            noticia.titulo = model.titulo;
        }

        if(typeof model.cuerpo != 'undefined' && model.cuerpo != ''){
            noticia.cuerpo = model.cuerpo;
        }
        return Promise.resolve(noticia);
    })
    .then((noticia) => {
        return new Promise((resolve, reject) => {
            noticia.save((err) => {
                if(err){
                    reject(err);
                }else{
                    resolve();
                }
            });
        });
    });
};

Noticia.prototype.removeFromBD = function(id) {
    return new Promise((resolve, reject) => {
        Schema.findById(String(id), (err, noticia) => {
            if(err){
                err.mycode = 0;
                reject(err);
            }else if(!noticia){
                let err = {mycode:1};
                reject(err);
            }else{
                resolve(noticia);
            }
        });
    })
    .then((noticia) => {
        return new Promise((resolve, reject) => {
            noticia.remove((err) => {
                if(err) {
                    err.mycode = 0;
                    reject(err);
                }else{
                    resolve();
                }
            });
        })
    })
};

Noticia.prototype.removeImage = function(id) {
    return new Promise((resolve, reject) => {
        this.cloudinary.api.delete_resources(ids, (result) => {
            resolve();
        });
    });
};

Noticia.prototype.remove = function(id) {
    return this.removeFromBD(id)
    .then(() => {
        return this.removeImage(id);
    });
};

export default Noticia;
