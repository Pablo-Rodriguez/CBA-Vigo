//Modelo do usuario
import bcrypt from 'bcryptjs';
import Schema from './schemas/usuario.js';

let Usuario = function (config) {
    config = config || {};
};

Usuario.prototype.all = function(page=0) {
    return Schema.find().skip(page);
};

Usuario.prototype.byName = function(name) {
    return Schema.findOne({name: name});
};

Usuario.prototype.new = function(model) {
    let salt, hash;
    if(model.password){
        salt = bcrypt.genSaltSync(10);
        hash = bcrypt.hashSync(model.password, salt);
    }

    let user = new Schema({
        username: model.username,
        password: hash,
        rol: model.rol
    });
    return new Promise((resolve, reject) => {
        user.save(function (err) {
            if(err){
                reject(err)
            }else{
                resolve();
            }
        });
    });
};

Usuario.prototype.remove = function(id) {
    return new Promise((resolve, reject) => {
        Schema.findById(String(id), (err, usuario) => {
            if(err){
                reject(err);
            }else if(!usuario){
                let err = {mycode:1};
                reject(err);
            }else{
                resolve(usuario);
            }
        })
        .then((usuario) => {
            return new Promise((resolve, reject) => {
                usuario.remove((err) => {
                    if(err){
                        reject(err);
                    }else{
                        resolve();
                    }
                });
            });
        });
    })
};

export default Usuario;