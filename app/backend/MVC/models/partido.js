//Modelo do partido
import Schema from './schemas/partido.js';

let Partido = function (config) {
    config = config || {};
};

Partido.prototype.all = function(page=0) {
    return Schema.find().skip(page);
};

Partido.prototype.new = function(model) {
    let partido = new Schema({
        info: model.info,
        equipo: model.equipo
    });

    return new Promise((resolve, reject) => {
        partido.save((err) => {
            if(err){
                reject(err);
            }else{
                resolve();
            }
        });
    });
};

Partido.prototype.update = function(model) {
    return new Promise((resolve, reject) => {
        Schema.findById(String(model.id), (err, partido) => {
            if(err){
                reject(err);
            }else if(!partido){
                err.mycode = 1;
                reject(err);
            }else{
                resolve(partido);
            }
        });
    })
    .then((partido) => {
        if(typeof req.body.info != 'undefined' && req.body.info != ''){
            match.info = req.body.info;
        }
        if(typeof req.body.equipo != 'undefined' && req.body.equipo != ''){
            match.equipo = req.body.equipo;
        }
        return partido;
    })
    .then((partido) => {
        partido.save((err) => {
            if(err){
                reject(err);
            }else{
                resolve();
            }
        });
    });
};

Partido.prototype.remove = function(id) {
    return new Promise((resolve, reject) => {
        Schema.findById(String(id), (err, partido) => {
            if(err){
                reject(err);
            }else if(!partido){
                let err = {mycode:1};
            }else{
                resolve(partido);
            }
        });
    })
    .then((partido) => {
        return new Promise((resolve, reject) => {
            partido.remove((err) => {
                if(err){
                    reject(err);
                }else{
                    resolve();
                }
            });
        });
    });
};

export default Partido;