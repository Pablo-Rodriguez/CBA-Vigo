import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let user = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    rol: {
        type: String,
        required: true
    }
});

export default mongoose.model('User', user);