import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let match = new Schema({
    info: {
        type: String,
        required: true
    },
    equipo: {
        type: String,
        required: true,
        enum: ['a', 'A', 'b', 'B']
    }
});

export default mongoose.model('Match', match);