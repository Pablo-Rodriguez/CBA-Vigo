import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let nova = new Schema({
    src: String,
    alt: String,
    title: String,
    titulo: String,
    cuerpo: String
});

export default mongoose.model('Nova', nova);