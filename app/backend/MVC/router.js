import Admin from './controllers/admin.js';
import Partido from './controllers/partido.js';
import Noticia from './controllers/noticia.js';
import Usuario from './controllers/usuario.js';
import Defaults from './controllers/defaults.js';
export default {
    admin: Admin,
    partidos: Partido,
    noticias: Noticia,
    usuarios: Usuario,
    '*': Defaults
}