var gulp = require('gulp'),
	browserify = require('browserify'),
	babelify = require('babelify'),
	buffer = require('vinyl-buffer'),
	source = require('vinyl-source-stream'),
	uglify = require('gulp-uglify'),
	stylus = require('gulp-stylus'),
	nib = require('nib'),
	minifyHTML = require('gulp-minify-html');

function browserifyOrigin (fileName) {
	return browserify({
		entries: './app/frontend/js/' + fileName,
		debug: true,
		transform: [babelify]
	}).bundle()
		.pipe(source(fileName))
		.pipe(buffer())
		//.pipe(uglify())
		.pipe(gulp.dest('./app/backend/public/js'));
}

gulp.task('html', function () {
	return gulp.src('app/frontend/index.html')
		.pipe(minifyHTML())
		.pipe(gulp.dest('app/backend/public/'));
});

gulp.task('fonts', function () {
	return gulp.src('app/frontend/fonts/*')
		.pipe(gulp.dest('app/backend/public/fonts'));
});

gulp.task('images', function () {
	return gulp.src('app/frontend/img/**/*')
		.pipe(gulp.dest('app/backend/public/img'));
});

gulp.task('css', function () {
	return gulp.src('./app/frontend/stylus/*')
		.pipe(stylus({
			compress: true,
			use: [nib()]
		}))
		.pipe(gulp.dest('./app/backend/public/css/'));

});

var JSs = ['main.js', 'admin.js'];

gulp.task('js', function () {
	JSs.map(function (fileName) {
		return browserifyOrigin(fileName);
	});
	return Promise.all(JSs);
});

gulp.task('watch', function () {
	gulp.watch('app/frontend/index.html', ['html']);
	gulp.watch('app/frontend/stylus/**/*.styl', ['css']);
	gulp.watch('app/frontend/js/**/*.js', ['js']);
});

gulp.task('default', ['html', 'css', 'js', 'fonts', 'images', 'watch']);
